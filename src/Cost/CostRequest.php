<?php
namespace LfConnector\Cost;

use LfConnector\Request\Request;
use LfConnector\Cost\CostInterface;


abstract class CostRequest extends Request implements CostInterface {
    /**
     * Se arma el payload con los componentes del CostInterface.
     *
     * @return array
     */
    public final function payload(): array
    {
        return [
            'lead' => $this->lead(),
            'cost' => $this->cost(),
            'currency' => $this->currency(),
        ];
    }

    /**
     * Devuelve el endpoint del Request.
     * @return string
     */

    public function endpoint(): string
    {
        return '/api/leads/cost.json';
    }
}