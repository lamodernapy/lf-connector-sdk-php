<?php

namespace LfConnector\Cost;

use LfConnector\Cost\CostRequest;
use LfConnector\Request\AuthorizationTrait;


class Cost extends CostRequest{
    use AuthorizationTrait;

    /**
     * Setter para el lead hash.
     * 
     * @param string $lead
     * @return void
     */

    public function setLead(string $lead): void
    {
        $this->lead = $lead;
    }
    
    /**
     * Setter para el currency.
     * 
     * @param string $currency
     * @return void
     */

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }
    
    /**
     * Setter para el cost.
     * 
     * @param float $cost
     * @return void
     */

    public function setCost(float $cost): void
    {
        $this->cost = $cost;
    }

    /**
     * Provee el lead hash.
     * @return string
     */

    public function lead(): string
    {
        return $this->lead ?? '';
    }

    /**
     * Provee el costo.
     * @return float
     */

    public function cost(): float
    {
        return $this->cost ?? 0;
    }

    /**
     * Provee el currency.
     * @return string
     */

    public function currency(): string
    {
        return $this->currency ?? '';
    }

    /** @var string */
    private $lead;

    /** @var string */
    private $currency;

    /** @var float */
    private $cost;
}