<?php

namespace LfConnector\Cost;

interface CostInterface {

    /**
     * El lead hash a enviar.
     * @return string
     */

    function lead(): string;

    /**
     * Provee la moneda del costo asociado.
     * @return string
     */

    function currency(): string;

    /**
     * Provee el costo del lead.
     * @return float
     */

    function cost(): float;

}