<?php

namespace LfConnector\Cost;

use LfConnector\Cost\Cost;
use LfConnector\Cost\CostFactory;

class CostFactory{
    /** @var Cost */
    private $Cost;

    /** @var array */
    const ALLOWED_FIELDS = [
        'lead', 'cost', 'currency'
    ];

    /**
     * Genera una nueva instancia del Factory.
     * @param array $data
     * @return CostFactory
     */

    public static function build(array $data = []): CostFactory
    {
        $factory = new CostFactory();
        $factory->Cost = new Cost();

        foreach(self::ALLOWED_FIELDS as $field){
            if(isset($data[$field])){
                $factory->{$field}($data[$field]);
            }
        }

        return $factory;
    }

    /**
     * Setea la autorizacion en el Factory.
     * 
     * @param string $authorization
     * @return CostFactory
     */

    public function authorization(string $authorization): CostFactory
    {
        $this->Cost->setAuthorization($authorization);
        return $this;
    }

    /**
     * Setea el costo en el Factory.
     * 
     * @param float $cost
     * @return CostFactory
     */

    public function cost(float $cost): CostFactory
    {
        $this->Cost->setCost($cost);
        return $this;
    }

    /**
     * Setea el currency en el Factory.
     * 
     * @param string $currency
     * @return CostFactory
     */

    public function currency(string $currency): CostFactory
    {
        $this->Cost->setCurrency($currency);
        return $this;
    }

    /**
     * Setea el lead hash en el Factory.
     * 
     * @param string $lead
     * @return CostFactory
     */

    public function lead(string $lead): CostFactory
    {
        $this->Cost->setLead($lead);
        return $this;
    }

    /**
     * Devuelve el conector configurado.
     * 
     * @return Cost
     */

    public function make(): Cost
    {
        return $this->Cost;
    }
}