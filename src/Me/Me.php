<?php

namespace LfConnector\Me;

use LfConnector\Me\MeRequest;
use LfConnector\Request\AuthorizationTrait;


class Me extends MeRequest{
    use AuthorizationTrait;
}