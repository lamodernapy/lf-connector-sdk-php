<?php
namespace LfConnector\Me;

use LfConnector\Request\Request;
use LfConnector\Me\MeInterface;


abstract class MeRequest extends Request implements MeInterface {
    /**
     * Se arma el payload con los componentes del MeInterface.
     *
     * @return array
     */
    public final function payload(): array
    {
        return [];
    }

    /**
     * Devuelve el endpoint del Request.
     * @return string
     */

    public function endpoint(): string
    {
        return '/api/auth/me.json';
    }
}