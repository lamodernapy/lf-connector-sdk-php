<?php

namespace LfConnector\Me;

use LfConnector\Me\Me;
use LfConnector\Me\MeFactory;

class MeFactory{
    /** @var Me */
    private $Me;

    /** @var array */
    const ALLOWED_FIELDS = [
        
    ];

    /**
     * Genera una nueva instancia del Factory.
     * @param array $data
     * @return MeFactory
     */

    public static function build(array $data = []): MeFactory
    {
        $factory = new MeFactory();
        $factory->Me = new Me();
        $factory->Me->method('GET');

        foreach(self::ALLOWED_FIELDS as $field){
            if(isset($data[$field])){
                $factory->{$field}($data[$field]);
            }
        }

        return $factory;
    }

    /**
     * Setea la autorizacion en el Factory.
     * 
     * @param string $authorization
     * @return MeFactory
     */

    public function authorization(string $authorization): MeFactory
    {
        $this->Me->setAuthorization($authorization);
        return $this;
    }

    /**
     * Devuelve el conector configurado.
     * 
     * @return Me
     */

    public function make(): Me
    {
        return $this->Me;
    }
}