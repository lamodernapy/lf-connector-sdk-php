<?php

namespace LfConnector\Request;

use Dotenv\Dotenv;
use LfConnector\Request\RequestInterface;

abstract class Request implements RequestInterface{

    /** @var string */
    private $method = 'POST';

    /** @var string */
    private $lang = null;

    /** @var string */
    private $timezone = null;

    /** 
     * Setea el metodo.
     * 
     * @param string $method
     * @return Request
     */

    public function method(string $method): Request
    {
        $this->method = $method;
        return $this;
    }

    /** 
     * Setea el lang.
     * 
     * @param string $lang
     * @return Request
     */

    public function lang(string $lang): Request
    {
        $this->lang = $lang;
        return $this;
    }

    /** 
     * Setea el timezone.
     * 
     * @param string $timezone
     * @return Request
     */

    public function timezone(string $timezone): Request
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * Efectua el request.
     * 
     * @return Request
     */

    public final function request(): Request
    {
        $payload = $this->payload();
        $authorization = $this->authorization();

        $json = json_encode($payload);

        $headers = [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json)
        ];

        if(strlen($authorization) > 0){
            $headers[] = 'Authorization: Bearer ' . $authorization;
        }

        if(isset($this->timezone)){
            $headers[] = 'X-Timezone-Offset: ' . $this->timezone;
        }

        if(isset($this->lang)){
            $headers[] = 'Accept-Language: ' . $this->lang;
        }

        $this->curl = curl_init($this->url());
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $this->method);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $json);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_HEADERFUNCTION, [ &$this, 'responseHeaders' ]);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);

        $this->headers = [];
        $result = curl_exec($this->curl);
        $this->response = json_decode($result, true);
        $this->status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        return $this;
    }

    /**
     * Procesa las cabeceras de respuesta.
     * 
     * @param mixed $curl
     * @param string $line
     * @return int
     */

    private function responseHeaders($curl,string $line): int
    {
        $header = explode(':', $line);
        $name = $header[0] ?? null;
        $value = $header[1] ?? null;
        
        if(isset($name)){
            $tname = trim($name);

            if(strlen($tname)){
                $this->headers[$tname] = trim($value);
            }
        }

        return strlen($line);
    }

    /**
     * Devuelve el Status Code del ultimo Request.
     * @return int
     */

    public function status(): int 
    {
        return $this->status ?? 0;
    }

    /**
     * Devuelve la Respuesta del ultimo Request.
     * @return array
     */

    public function response(): array 
    {
        return $this->response ?? [];
    }

    /**
     * Devuelve las cabeceras del ultimo Request.
     * @return array
     */

    public function headers(): array 
    {
        return $this->headers ?? [];
    }

    /**
     * Devuelve la URL del Request.
     * @return string
     */

    const PRODUCTION_URL = 'https://leadfarm.agenciamoderna.com.py';

    public function url(): string 
    {
        $url = self::env('LF_API_URL');

        if(! $url){
            $url = self::PRODUCTION_URL;
        }

        return $url . $this->endpoint();
    }

    /** 
     * Inicializa el singleton Dotenv. Y lee el key solicitado.
     * @param string $key
     * @return string
     */

    protected static function env(string $key): string
    {
        $env = getenv($key);

        if(! $env){
            if (is_null(self::$_dotEnv)){
                $dirs = [
                    __DIR__ . '/../../../../..',
                    __DIR__ . '/../..'
                ];

                foreach($dirs as $dir){
                    if(file_exists($dir . '/.env')){
                        self::$_dotEnv = Dotenv::create($dir);
                        self::$_dotEnv->load();
                        break;
                    }
                }
            }
            
            $env = getenv($key);
        }

        return $env;
    }
    
    /** @var ?Dotenv */
    private static $_dotEnv = null;

    /** @var int */
    private static $status = 0;

    /** @var array */
    private static $response = [];

    /** @var array */
    private static $headers = [];

    /** @var resource */
    private static $curl;

    /**
     * Devuelve la Respuesta en un array.
     * @return array
     */

    public function toArray(): array
    {
        return [
            'url' => $this->url(),
            'status' => $this->status(),
            'headers' => $this->headers(),
            'response' => $this->response(),
        ]; 
    }

}