<?php

namespace LfConnector\Request;

trait AuthorizationTrait {

    /**
     * Setter para la autorizacion.
     * 
     * @param string $authorization
     * @return void
     */

    public function setAuthorization(string $authorization): void
    {
        $this->authorization = $authorization;
    }

    /**
     * Provee la autorizacion.
     */

    public function authorization(): string
    {
        return $this->authorization ?? '';
    }

    /** @var ?string */
    private $authorization = null;
    
}