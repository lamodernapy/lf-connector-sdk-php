<?php

namespace LfConnector\Request;

interface RequestInterface {
    /** 
     * Resuelve el endpoint al cual hacer el request.
     * @return string
     */

    function endpoint(): string;

    
    /** 
     * Resuelve el payload a enviar.
     * @return array
     */

    function payload(): array;

    /** 
     * Resuelve el token de autenticacion.
     * @return string
     */

    function authorization(): string;

}