<?php

namespace LfConnector\Campaigns;

interface CampaignsInterface {

    /**
     * La pagina solicitada.
     * @return int
     */

    function page(): int;

}