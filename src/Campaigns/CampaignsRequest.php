<?php
namespace LfConnector\Campaigns;

use LfConnector\Request\Request;
use LfConnector\Campaigns\CampaignsInterface;


abstract class CampaignsRequest extends Request implements CampaignsInterface {
    /**
     * Se arma el payload con los componentes del CampaignsInterface.
     *
     * @return array
     */
    public final function payload(): array
    {
        return [
            'page' => $this->page()
        ];
    }

    /**
     * Devuelve el endpoint del Request.
     * @return string
     */

    public function endpoint(): string
    {
        return '/api/connector/campaigns.json';
    }
}