<?php

namespace LfConnector\Campaigns;

use LfConnector\Campaigns\CampaignsRequest;
use LfConnector\Request\AuthorizationTrait;


class Campaigns extends CampaignsRequest{
    use AuthorizationTrait;

    /**
     * Setter para el page.
     * 
     * @param int $page
     * @return void
     */

    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * Provee la pagina deseada.
     * @return int
     */

    public function page(): int
    {
        return $this->page ?? 1;
    }

    /** @var int */
    private $page;
}