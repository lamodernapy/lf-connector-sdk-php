<?php

namespace LfConnector\Campaigns;

use LfConnector\Campaigns\Campaigns;
use LfConnector\Campaigns\CampaignsFactory;

class CampaignsFactory{
    /** @var Campaigns */
    private $Campaigns;

    /** @var array */
    const ALLOWED_FIELDS = [
        'page'
    ];

    /**
     * Genera una nueva instancia del Factory.
     * @param array $data
     * @return CampaignsFactory
     */

    public static function build(array $data = []): CampaignsFactory
    {
        $factory = new CampaignsFactory();
        $factory->Campaigns = new Campaigns();
        $factory->Campaigns->method('GET');

        foreach(self::ALLOWED_FIELDS as $field){
            if(isset($data[$field])){
                $factory->{$field}($data[$field]);
            }
        }

        return $factory;
    }

    /**
     * Setea la autorizacion en el Factory.
     * 
     * @param string $authorization
     * @return CampaignsFactory
     */

    public function authorization(string $authorization): CampaignsFactory
    {
        $this->Campaigns->setAuthorization($authorization);
        return $this;
    }

    /**
     * Setea el page en el Factory.
     * 
     * @param int $page
     * @return CampaignsFactory
     */

    public function page(int $page): CampaignsFactory
    {
        $this->Campaigns->setPage($page);
        return $this;
    }

    /**
     * Devuelve el conector configurado.
     * 
     * @return Campaigns
     */

    public function make(): Campaigns
    {
        return $this->Campaigns;
    }
}