<?php
namespace LfConnector\Lead;

use LfConnector\Request\Request;
use LfConnector\Lead\LeadInterface;


abstract class LeadRequest extends Request implements LeadInterface {
    /**
     * Se arma el payload con los componentes del LeadInterface.
     *
     * @return array
     */
    public final function payload(): array
    {
        return [
            'lead' => $this->lead(),
            'context' => $this->context(),
        ];
    }

    /**
     * Devuelve el endpoint del Request.
     * @return string
     */

    public function endpoint(): string
    {
        return '/api/leads/feed.json';
    }
}