<?php

namespace LfConnector\Lead;

interface LeadInterface {

    /**
     * El lead a enviar.
     * @return array
     */

    function lead(): array;


    /**
     * Provee el contexto a enviar.
     * @return array
     */

    function context(): array;

}