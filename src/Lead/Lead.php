<?php

namespace LfConnector\Lead;

use LfConnector\Lead\LeadRequest;
use LfConnector\Request\AuthorizationTrait;


class Lead extends LeadRequest{
    use AuthorizationTrait;
    /**
     * Setter para el lead.
     * 
     * @param array $lead
     * @return void
     */

    public function setLead(array $lead): void
    {
        $this->lead = $lead;
    }
    
    /**
     * Setter para el contexto.
     * 
     * @param array $context
     * @return void
     */

    public function setContext(array $context): void
    {
        $this->context = $context;
    }

    /**
     * Provee el lead.
     */

    public function lead(): array
    {
        return $this->lead ?? [];
    }

    /**
     * Provee los contextos.
     */

    public function context(): array
    {
        return $this->context ?? [];
    }

    /** @var array */
    private $lead = [];

    /** @var array */
    private $context = [];
}