<?php

namespace LfConnector\Lead;

use LfConnector\Lead\Lead;
use LfConnector\Lead\LeadFactory;

class LeadFactory{
    /** @var Lead */
    private $Lead;

    /** @var array */
    const ALLOWED_FIELDS = [
        'lead', 'context', 'authorization'
    ];

    /**
     * Genera una nueva instancia del Factory.
     * @param array $data
     * @return LeadFactory
     */

    public static function build(array $data = []): LeadFactory
    {
        $factory = new LeadFactory();
        $factory->Lead = new Lead();

        foreach(self::ALLOWED_FIELDS as $field){
            if(isset($data[$field])){
                $factory->{$field}($data[$field]);
            }
        }

        return $factory;
    }

    /**
     * Setea la autorizacion en el Factory.
     * 
     * @param string $authorization
     * @return LeadFactory
     */

    public function authorization(string $authorization): LeadFactory
    {
        $this->Lead->setAuthorization($authorization);
        return $this;
    }

    /**
     * Setea el contexto en el Factory.
     * 
     * @param array $context
     * @return LeadFactory
     */

    public function context(array $context): LeadFactory
    {
        $this->Lead->setContext($context);
        return $this;
    }

    /**
     * Setea los campos en el Factory.
     * 
     * @param array $lead
     * @return LeadFactory
     */

    public function lead(array $lead): LeadFactory
    {
        $this->Lead->setLead($lead);
        return $this;
    }

    /**
     * Devuelve el conector configurado.
     * 
     * @return Lead
     */

    public function make(): Lead
    {
        return $this->Lead;
    }
}