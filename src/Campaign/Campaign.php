<?php

namespace LfConnector\Campaign;

use LfConnector\Campaign\CampaignRequest;
use LfConnector\Request\AuthorizationTrait;


class Campaign extends CampaignRequest{
    use AuthorizationTrait;

    /**
     * Setter para el id.
     * 
     * @param string $id
     * @return void
     */

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * Provee el ID.
     * @return string
     */

    public function id(): string
    {
        return $this->id ?? '';
    }

    /** @var string */
    private $id;
}