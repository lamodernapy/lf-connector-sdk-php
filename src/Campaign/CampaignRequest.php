<?php
namespace LfConnector\Campaign;

use LfConnector\Request\Request;
use LfConnector\Campaign\CampaignInterface;


abstract class CampaignRequest extends Request implements CampaignInterface {
    /**
     * Se arma el payload con los componentes del CampaignInterface.
     *
     * @return array
     */
    public final function payload(): array
    {
        return [
            'id' => $this->id()
        ];
    }

    /**
     * Devuelve el endpoint del Request.
     * @return string
     */

    public function endpoint(): string
    {
        return '/api/connector/campaign.json';
    }
}