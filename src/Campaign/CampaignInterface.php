<?php

namespace LfConnector\Campaign;

interface CampaignInterface {

    /**
     * El id solicitado.
     * @return string
     */

    function id(): string;

}