<?php

namespace LfConnector\Campaign;

use LfConnector\Campaign\Campaign;
use LfConnector\Campaign\CampaignFactory;

class CampaignFactory{
    /** @var Campaign */
    private $Campaign;

    /** @var array */
    const ALLOWED_FIELDS = [
        'id'
    ];

    /**
     * Genera una nueva instancia del Factory.
     * @param array $data
     * @return CampaignFactory
     */

    public static function build(array $data = []): CampaignFactory
    {
        $factory = new CampaignFactory();
        $factory->Campaign = new Campaign();
        $factory->Campaign->method('GET');

        foreach(self::ALLOWED_FIELDS as $field){
            if(isset($data[$field])){
                $factory->{$field}($data[$field]);
            }
        }

        return $factory;
    }

    /**
     * Setea la autorizacion en el Factory.
     * 
     * @param string $authorization
     * @return CampaignFactory
     */

    public function authorization(string $authorization): CampaignFactory
    {
        $this->Campaign->setAuthorization($authorization);
        return $this;
    }

    /**
     * Setea el id en el Factory.
     * 
     * @param string $id
     * @return CampaignFactory
     */

    public function id(string $id): CampaignFactory
    {
        $this->Campaign->setId($id);
        return $this;
    }

    /**
     * Devuelve el conector configurado.
     * 
     * @return Campaign
     */

    public function make(): Campaign
    {
        return $this->Campaign;
    }
}