<?php

namespace LfConnector\Login;

use LfConnector\Login\Login;
use LfConnector\Login\LoginFactory;


class LoginFactory{

    /** @var Login */
    private $login;

    /** @var array */
    const ALLOWED_FIELDS = [
        'email', 'password', 'product'
    ];

    /**
     * Genera una nueva instancia del Factory.
     * @param array $data
     * @return LoginFactory
     */

    public static function build(array $data = []): LoginFactory
    {
        $factory = new LoginFactory();
        $factory->login = new Login();

        foreach(self::ALLOWED_FIELDS as $field){
            if(isset($data[$field])){
                $factory->{$field}($data[$field]);
            }
        }

        return $factory;
    }
    
    /**
     * Setea el password del Factory.
     * 
     * @param string $password
     * @return LoginFactory
     */

    public function password(string $password): LoginFactory
    {
        $this->login->setPassword($password);
        return $this;
    }
    
    /**
     * Setea el email del Factory.
     * 
     * @param string $email
     * @return LoginFactory
     */

    public function email(string $email): LoginFactory
    {
        $this->login->setEmail($email);
        return $this;
    }

    /**
     * Setea el producto del Factory.
     * 
     * @param string $product
     * @return LoginFactory
     */

    public function product(string $product): LoginFactory
    {
        $this->login->setProduct($product);
        return $this;
    }

    /**
     * Devuelve el login configurado.
     * 
     * @return Login
     */

    public function make(): Login
    {
        return $this->login;
    }
}