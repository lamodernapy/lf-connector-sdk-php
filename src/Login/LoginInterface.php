<?php

namespace LfConnector\Login;

interface LoginInterface {

    /**
     * Provee el email del user a autenticar.
     * @return string
     */

    function email(): string;


    /**
     * Provee el password del user a autenticar.
     * @return string
     */

    function password(): string;

    /**
     * Provee el producto sobre el cual autenticarse.
     * @return string
     */

    function product(): string;
}