<?php

namespace LfConnector\Login;

use LfConnector\Login\LoginRequest;
use LfConnector\Request\AuthorizationTrait;

class Login extends LoginRequest{

    /**
     * Setter para producto
     * @param string $product
     * @return void
     */

    public function setProduct(string $product): void
    {
        $this->product = $product;
    }

    /**
     * Setter para email
     * @param string $email
     * @return void
     */

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * Setter para password
     * @param string $password
     * @return void
     */

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * Provee el Producto.
     * @return string
     */

    public function product(): string
    {
        return $this->product ?? '';
    }
    /**
     * Provee el Email.
     * @return string
     */

    public function email(): string
    {
        return $this->email ?? '';
    }

    /**
     * Provee el password.
     * @return string
     */

    public function password(): string
    {
        return $this->password ?? '';
    }

    /** @var ?string */
    private $password = null;

    /** @var ?string */
    private $email = null;

    /** @var ?string */
    private $product = null;

    /** 
     * No hay AUTH porque es el Login.
     * @return string
     */

    public function authorization(): string 
    {
        return '';
    }

}