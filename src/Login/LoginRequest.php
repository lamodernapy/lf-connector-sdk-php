<?php
namespace LfConnector\Login;

use LfConnector\Request\Request;
use LfConnector\Login\LoginInterface;


abstract class LoginRequest extends Request implements LoginInterface {
    /**
     * Se arma el payload con los componentes del Loginnterface.
     *
     * @return array
     */
    public final function payload(): array
    {
        return [
            'email' => $this->email(),
            'password' => $this->password(),
            'product' => $this->product(),
        ];
    }

    /**
     * Devuelve el endpoint del Request.
     * @return string
     */

    public function endpoint(): string
    {
        return '/api/auth/login.json';
    }
}