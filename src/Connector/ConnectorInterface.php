<?php

namespace LfConnector\Connector;

interface ConnectorInterface {

    /**
     * Provee los campos a conectar.
     * @return array
     */

    function fields(): array;


    /**
     * Provee los campos claves a conectar.
     * @return array
     */

    function keys(): array;

    /**
     * Provee los contextos a conectar.
     * @return array
     */

    function contexts(): array;

    /**
     * Provee el nombre del template a conectar.
     * @return string
     */

    function template(): string;

    /**
     * Provee el hash del campaign a conectar.
     * @return string
     */

    function campaign(): string;
}