<?php

namespace LfConnector\Connector;

use LfConnector\Connector\ConnectorRequest;
use LfConnector\Request\AuthorizationTrait;

class Connector extends ConnectorRequest{
    use AuthorizationTrait;

    /**
     * Setter para el los campos.
     * 
     * @param array $fields
     * @return void
     */

    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }
    
    /**
     * Setter para el los contextos.
     * 
     * @param array $contexts
     * @return void
     */

    public function setContexts(array $contexts): void
    {
        $this->contexts = $contexts;
    }
    
    /**
     * Setter para el los campos clave.
     * 
     * @param array $keys
     * @return void
     */

    public function setKeys(array $keys): void
    {
        $this->keys = $keys;
    }
    
    /**
     * Setter para el nombre del template.
     * 
     * @param string $template
     * @return void
     */

    public function setTemplate(string $template): void
    {
        $this->template = $template;
    }

    /**
     * Setter para el hash de campaign.
     * 
     * @param string $campaign
     * @return void
     */

    public function setCampaign(string $campaign): void
    {
        $this->campaign = $campaign;
    }

    /**
     * Provee los campos.
     */

    public function fields(): array
    {
        return $this->fields ?? [];
    }

    /**
     * Provee los contextos.
     */

    public function contexts(): array
    {
        return $this->contexts ?? [];
    }

    /**
     * Provee los campos clave.
     */

    public function keys(): array
    {
        return $this->keys ?? [];
    }
    /**
     * Provee el nombre del Template.
     */

    public function template(): string
    {
        return $this->template ?? '';
    }

    /**
     * Provee el hash de Campaign.
     */

    public function campaign(): string
    {
        return $this->campaign ?? '';
    }

    /** @var array */
    private $fields = [];

    /** @var array */
    private $contexts = [];

    /** @var array */
    private $keys = [];

    /** @var ?string */
    private $template = null;

    /** @var ?string */
    private $campaign = null;

}