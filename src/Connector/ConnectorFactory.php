<?php

namespace LfConnector\Connector;

use LfConnector\Connector\Connector;
use LfConnector\Connector\ConnectorFactory;


class ConnectorFactory{
    /** @var Connector */
    private $connector;

    /** @var array */
    const ALLOWED_FIELDS = [
        'key', 'fields', 'contexts', 'template', 'authorization', 'campaign'
    ];

    /**
     * Genera una nueva instancia del Factory.
     * @param array $data
     * @return ConnectorFactory
     */

    public static function build(array $data = []): ConnectorFactory
    {
        $factory = new ConnectorFactory();
        $factory->connector = new Connector();

        foreach(self::ALLOWED_FIELDS as $field){
            if(isset($data[$field])){
                $factory->{$field}($data[$field]);
            }
        }

        return $factory;
    }

    /**
     * Setea la autorizacion en el Factory.
     * 
     * @param string $authorization
     * @return ConnectorFactory
     */

    public function authorization(string $authorization): ConnectorFactory
    {
        $this->connector->setAuthorization($authorization);
        return $this;
    }

    /**
     * Setea los campos clave en el Factory.
     * 
     * @param array $keys
     * @return ConnectorFactory
     */

    public function keys(array $keys): ConnectorFactory
    {
        $this->connector->setKeys($keys);
        return $this;
    }

    /**
     * Setea el hash del campaign en el Factory.
     * 
     * @param string $campaign
     * @return ConnectorFactory
     */

    public function campaign(string $campaign): ConnectorFactory
    {
        $this->connector->setCampaign($campaign);
        return $this;
    }

    /**
     * Setea el nombre del template en el Factory.
     * 
     * @param string $template
     * @return ConnectorFactory
     */

    public function template(string $template): ConnectorFactory
    {
        $this->connector->setTemplate($template);
        return $this;
    }

    /**
     * Setea los contextos en el Factory.
     * 
     * @param array $contexts
     * @return ConnectorFactory
     */

    public function contexts(array $contexts): ConnectorFactory
    {
        $this->connector->setContexts($contexts);
        return $this;
    }

    /**
     * Setea los campos en el Factory.
     * 
     * @param array $fields
     * @return ConnectorFactory
     */

    public function fields(array $fields): ConnectorFactory
    {
        $this->connector->setFields($fields);
        return $this;
    }

    /**
     * Devuelve el conector configurado.
     * 
     * @return Connector
     */

    public function make(): Connector
    {
        return $this->connector;
    }
}