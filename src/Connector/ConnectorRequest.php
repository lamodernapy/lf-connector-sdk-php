<?php
namespace LfConnector\Connector;

use LfConnector\Request\Request;
use LfConnector\Connector\ConnectorInterface;


abstract class ConnectorRequest extends Request implements ConnectorInterface {
    /**
     * Se arma el payload con los componentes del ConnectorInterface.
     *
     * @return array
     */
    public final function payload(): array
    {
        return [
            'fields' => $this->fields(),
            'keys' => $this->keys(),
            'contexts' => $this->contexts(),
            'template' => $this->template(),
            'campaign' => $this->campaign()
        ];
    }

    /**
     * Devuelve el endpoint del Request.
     * @return string
     */

    public function endpoint(): string
    {
        return '/api/connector/sync.json';
    }
}