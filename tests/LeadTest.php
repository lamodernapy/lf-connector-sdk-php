<?php 

use LfConnector\Lead\LeadFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$lead = LeadFactory::build()
                    ->lead([ 
                        'email' => 'felipe@agenciamoderna.com.py',
                        'name' => 'Felipe Martinez',
                        'telephone' => '0985714373',
                        'school' => 'CTN',
                        'career' => 'Sistemas',
                    ])
                    ->context([ 'source' => 'UAA Landing', 'platform' => 'web' ])
                    ->authorization('ae2e983c-1970-4ff0-b022-5c6cad322022');

print_r($lead->make()->request()->toArray());