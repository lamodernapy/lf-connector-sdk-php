<?php 

use LfConnector\Login\LoginFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$login = LoginFactory::build()
                    ->email('felipeklez@gmail.com')
                    ->password('12345')
                    ->product('FB-CONN');

print_r($login->make()->request()->toArray());