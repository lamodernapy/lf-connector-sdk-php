<?php 

use LfConnector\Campaign\CampaignFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$campaign = CampaignFactory::build()
                    ->id('f971ec238a6c2b1c7104a981cdbbe6f10740f389b9011f7d6143b7397ccd7ad9')
                    ->authorization('37ac638a-8a0c-4afa-b880-bac8ebfbe485:6ccf4b154ff5c89b49901a82e089af9ab57212851856822ae62b2c0074b34952');

print_r($campaign->make()->request()->toArray());