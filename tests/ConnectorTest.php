<?php 

use LfConnector\Connector\ConnectorFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$connector = ConnectorFactory::build()
                    ->fields([ 'email', 'full_name'])
                    ->keys([ 'email'])
                    ->template('Template Testing')
                    ->intent('Intent Testing')
                    ->contexts([
                        [ 'source' => 'fl-sdk', 'content' => 'TEST1' ],
                        [ 'source' => 'fl-sdk', 'content' => 'TEST2' ]
                    ])
                    ->authorization('3e29528a-4487-4589-adf8-f60117638a3a:42a268fa47c6422e69357292a222f231f6bb871c904bc7d220335336eab3e037');

print_r($connector->make()->request()->toArray());