<?php 

use LfConnector\Cost\CostFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$cost = CostFactory::build()
                    ->lead('95c78949d0d837fcc4ec2f9db3ebd70b81b8672357cada44c9042006093ffa99')
                    ->currency('GS')
                    ->cost(1.90901)
                    ->authorization('dd355726-4c8c-41c5-842a-ae504b1e5c3a');

print_r($cost->make()->request()->toArray());