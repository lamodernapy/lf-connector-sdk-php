<?php 

use LfConnector\Lead\LeadFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$lead = LeadFactory::build()
                    ->lead([ 'email' => 'felipe', 'full_name' => 'Felipe Martinez'])
                    ->context([ 'source' => 'fl-sdk', 'content' => 'TEST1' ])
                    ->authorization('9f399e4f-4f8a-4f53-83e1-6f6ca0367b5a');

print_r($lead->make()->lang('es')->timezone('Asia/Tokyo')->request()->toArray());