<?php 

use LfConnector\Me\MeFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$me = MeFactory::build()->authorization('9980e2de-7e2b-420e-a8a7-933469d0bb94');

print_r($me->make()->method('GET')->request()->toArray());