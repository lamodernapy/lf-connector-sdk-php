<?php 

use LfConnector\Campaigns\CampaignsFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$campaigns = CampaignsFactory::build()
                    ->page(1)
                    ->authorization('9980e2de-7e2b-420e-a8a7-933469d0bb94:42a268fa47c6422e69357292a222f231f6bb871c904bc7d220335336eab3e037');

print_r($campaigns->make()->request()->toArray());